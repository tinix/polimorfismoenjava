package ejemplopolimorfismo;

/**
 *
 * @author tinix
 */
public class EjemploPolimorfismo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Empleado empl = new Empleado("Tinix" , 1000);
        imprimirDetalles(empl);
        
        Gerente gerent = new Gerente("Carla", 2000, "Finanzas");
        imprimirDetalles(gerent);
       
    }
    
    public static void imprimirDetalles(Empleado empl){
        System.out.println(empl.obtenerDetalles());
    }
    
}
